# Projet Infra

## Sommaire 

- [Projet Infra](#projet-Infra)
  - [Sommaire](#sommaire)
  - [Présentation du besoin](#Présentation-du-besoin)
  - [Présentation des solutions](#Présentation-des-soltuions)
    - [Mise en place des soltuions](#Mise-en-place-des-soltuions)
      - [Serveur Web](#Serveur-Web)
      - [Serveur Rsyslog](#etablir-un-serveur-Rsyslog)
      - [Reverse Proxy](#Reverse-proxy)
      - [Monitoring et backup](#Monitoring)
      - [Firewall ufw](#Firewall-ufw)
      
      


## Présentation du besoin

Nous allons à travers ce projet, héberger une plateforme multimédia en ligne puis la sécurisé. 

## Présentation des soltuions

Pour se faire, nous allons dans un premier temps installer notre plateforme multimédia nommé Jellyfin. Ensuite, nous metterons en place un serveur web Apache pour pouvoir héberger notre site.

Puis, nous ferons un reverse proxy Nginx et enfin un serveur Syslog avec Rsyslog pour récupérer les logs de nos machines. Enfin, pour ajouter une couche de sécurité à notre réseau, on mettra en place un parefeu avec UFW.

- Schéma de nos machines

 3 machines sous Debian, avec 2 giga de RAM et 10GB CPU.

![](schema.png)
    
## Mise en place des solutions

### Installation et configuration serveur web et Jellyfin

#### - Installation Jellyfin

```
# Sur GNU/Linux :
sudo apt install extrepo
sudo extrepo enable jellyfin
sudo apt update

sudo apt install jellyfin
sudo service jellyfin status
sudo systemctld restart jellyfin

# Pour désactiver jellyfin
sudo /etc/init.d/jellyfin stop

```

#### - Accès au Dashboard

A travers l'adresse ```http://localhost:8096```

### Etablir Serveur syslog

Rsyslog est un service qui va nous permettre de centraliser les logs de nos machines. Cela permet d'ajouter une couche de sécurité, de praticité à nos machines.

Pour ce projet, nous choisierons le protocole TCP puisqu'il est plus fiable que l'UDP.
Le service écoutera donc sur le port 10154.
Notons qu'il est possible d'utiliser les deux protocoles, mais il faudra utiliser deux ports différents.

#### - Configuration Rsyslog et centralisation des logs

- Côté serveur

Le service Rsyslog est installé par défaut sous les distributions GNU/Linux.

Nous allons quand même vérifier à travers la commande ```find -iname rsyslog``` si il existe.

Si il ne l'est pas :

``` 
sudo apt install rsyslog

```
Une fois le service installé, on se rend dans le fichier de configuration **etc/rsyslog.conf** et activer le port qui nous intéresse.

On va activer le port d'écoute TCP :
``` 
#Provides TCP syslog reception
$ModLoad imtcp
$InputTCPServerRun 10514 
```
Nous allons ensuite ajouter une règle dans le fichier de configuration, dans RULES.
Celle-ci va créer, pour chaque machine, un dossier dans lequel se trouvera un fichier *syslog.log*, où seront consutable nos logs.

```
vim /etc/rsyslog.conf
$template syslog,"/var/log/clients/%fromhost%/syslog.log"*.* ?syslog
```
Une fois notre fichier configuré, on active notre service.
```
systemctl enable rsyslog
systemctl start rsyslog
```
Notre port est bien en écoute on le vérifie ainsi :
```
[root@srv-log ~]# ss -utln
tcp   LISTEN     0      25                     *:10514    
```

- Côté client 

On vérifie, si notre service est installé et activé.

Ensuite, on se rend dans le fichier de configuration, on ajoute à la fin de page avec l'adresse IP serveur :
```
vim /etc/rsyslog.conf
*.* @@192.168.0.30:10514
```
```
systemctl restart rsyslog
```
Notre serveur et nos clients sont maintenant bien configurés.

### Etablir un reverse proxy

Nous utiliserons NGINX pour établir notre reverse proxy.
Ce service au valable par défaut sous GNU/Linux.

#### - Configuration Nginx

On va créer un fichier de configuration pour notre site web.
```
vim /etc/nginx/sites-available/jellyfin.com.conf
```
On y inscrit ensuite 
```
upstream monsite1 {
    server 192.168.159.129;
}

server {
    server_name jellyfin.com;

    location / {
        proxy_pass http://192.168.159.129:8096;
        proxy_set_header    Host $host;
        
        proxy_connect_timeout 30;
        proxy_send_timeout 30;
    }
}
```
Nous allons maintenant activer notre site au yeux de Nginx en créant un lien symbolique.

```ln -s /etc/nginx/sites-available/jellyfin.com.conf /etc/nginx/sites-enabled/jellyfin.com.conf```

On vérifie en détail avec ``` journalctl -xe -u nginx ``` si notre service est bien actif.

Ensuite, pour chacun de nos clients, nous préciseront l'adresse ip du reverse proxy, soit ```192.168.0.128``` en résolution DNS.

### Monitoring avec Netdata

  #### - Installation
   
  Dans un terminal avec ```wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh```

  #### - Accès au Dashboard
  Avec l'adresse ```http://localhost:19999```

  #### - Configuration du script afin de recevoir les notifications de Netdata sur Discord

    - Sur Discord :
Server settings > Integrations > Create Webhook
Copier le lien du Webhook
    - Sur serveur : 
   **/etc/netdata/edit-config health_alarm_notify.conf** est le fichier de configuration de Netdata
  #### - Le script
  Dans le fichier de configuration, y insérer le script :
  ```
  # sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD=**"YES"**

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="(COLLER LE LIEN DU WEBHOOK ICI)"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

#### Backup 
Il a été mis en place un script qui effectue une copie des fichiers à sauvegarder de manière récurrente. 

Dans un premier temps, les différents fichiers de configuration des applications utilisées **(/etc/*.conf)** sont copiées vers un fichier **/saveconfig** situé à la racine de notre machine hébergeant notre serveur web. 

Puis, les données liés à ces applications sont copiées et conservées dans un fichier **/saveappdata**.

### Firewall avec UFW

#### - Installation
Installer et activer notre outil, pour toutes nos machines sur le réseau.
```
apt-get update && apt-get install ufw
ufw enable
```
Nous allons ensuite, faire en sorte que rien ne rentre et rien ne sort, pour ensuite attribuer les ports voulus.
C'est à dire, les ports ssh (22), internet (80), notre site (8096), dns (53), https (443), 10514(tcp), 19999(netdata)
```
ufw default deny incoming 
ufw default deny outgoing  
ufw logging on
ufw allow out 22/tcp 
ufw allow in 22/tcp 
ufw allow out 80/tcp 
ufw allow out 53/tcp 
ufw allow out 443/tcp 
ufw allow out 1514/tcp
ufw allow out 19999/tcp 
```
Pour appliquer les changements 
```ufw disable && ufw enable```

Pour supprimer une règle
```ufw delete allow out 80/tcp```


### Conclusion

Notre réseau est maintenant configuré.
